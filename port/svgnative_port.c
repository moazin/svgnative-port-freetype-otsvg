#include <SVGNativeCAPI.h>
#include <cairo.h>

#include <svgnative_port.h>
#include <ft2build.h>
#include <math.h>
#include <stdlib.h>
#include FT_FREETYPE_H
#include FT_INTERNAL_TRUETYPE_TYPES_H
#include FT_BBOX_H
#include FT_SVG_RENDERER_H

  FT_Error
  svgnative_port_init( FT_Library  library  )
  {
    return FT_Err_Ok;
  }

  void
  svgnative_port_free( FT_Library  library )
  {
    /* nothing to do here */
  }

  FT_Error
  svgnative_port_render( FT_GlyphSlot slot, FT_BBox  outline_bbox )
  {
    FT_SVG_Document  document;
    FT_Size_Metrics  metrics;
    FT_UShort        units_per_EM;
    FT_Error         ft_error;

    unsigned int    glyph_index;
    unsigned short  start_glyph_id;
    unsigned short  end_glyph_id;
    float           x_svg_to_out;
    float           y_svg_to_out;
    float           x_outline_to_out;
    float           y_outline_to_out;
    float           x_outline_to_svg;
    float           y_outline_to_svg;
    unsigned char   *buffer;
    int             width;
    int             height;
    int             pitch;
    int             width_final;
    int             height_final;
    int             stride;
    char*           document_str;

    cairo_surface_t    *surface;
    cairo_t            *cr;
    cairo_rectangle_t  bbox_svg;
    cairo_rectangle_t  box; /* viewbox */
    SVGNative_Hive     hive;

    Svgnative_Port_State  state;

    state = (Svgnative_Port_State)slot->library->svg_renderer_state;
    document = (FT_SVG_Document)slot->other;

    metrics        = document->metrics;
    units_per_EM   = document->units_per_EM;
    start_glyph_id = document->start_glyph_id;
    end_glyph_id   = document->end_glyph_id;
    glyph_index    = slot->glyph_index;

    /* TODO: (OT-SVG)
     * create a copy of the string and add a null byte
     * terminator at the end. The thing is SVG native
     * doesn't behave fine if null character doesn't
     * exist, as it doesn't take in the length through
     * args either. A better fix might be to actually
     * do this on FT side so that the document is
     * loaded with a null byte at the end. Maybe later.
     */
    document_str = (char*)malloc( document->svg_document_length + 1 );
    memcpy( (void*)document_str,
            (const void*)document->svg_document,
            document->svg_document_length );
    document_str[document->svg_document_length] = '\0';

    hive = svgnative_hive_create();
    svgnative_hive_install_renderer( hive, RENDER_CAIRO );
    svgnative_hive_install_document_from_buffer( hive, document_str );
    free( document_str );

    box.width  = svgnative_hive_get_width_from_installed_document( hive );
    box.height = svgnative_hive_get_height_from_installed_document( hive );

    /* scale factor from SVG coordinates -> Output */
    x_svg_to_out = (float)metrics.x_ppem /(float)box.width;
    y_svg_to_out = (float)metrics.y_ppem /(float)box.height;

    /* scale factor from Outlines -> Output */
    x_outline_to_out = (float)metrics.x_ppem/(float)units_per_EM;
    y_outline_to_out = (float)metrics.y_ppem/(float)units_per_EM;

    /* scale factor from outline -> svg */
    x_outline_to_svg = (float)box.width/(float)units_per_EM;
    y_outline_to_svg = (float)box.height/(float)units_per_EM;

    bbox_svg.x        =  ( outline_bbox.xMin * x_outline_to_svg );
    bbox_svg.y        =  -1*( outline_bbox.yMax * y_outline_to_svg );
    bbox_svg.width    = ( outline_bbox.xMax - outline_bbox.xMin ) * x_outline_to_svg;
    bbox_svg.height   = ( outline_bbox.yMax - outline_bbox.yMin ) * y_outline_to_svg;

    width_final  = ceil( ( outline_bbox.xMax - outline_bbox.xMin )
                         * x_outline_to_out );
    height_final = ceil( ( outline_bbox.yMax - outline_bbox.yMin )
                         * y_outline_to_out );
    stride       = cairo_format_stride_for_width( CAIRO_FORMAT_ARGB32,
                                                  width_final );
    state->surface = cairo_image_surface_create_for_data( slot->bitmap.buffer,
                                                         CAIRO_FORMAT_ARGB32,
                                                         width_final,
                                                         height_final,
                                                         stride );
    state->cr      = cairo_create(state->surface);
    svgnative_hive_install_output( hive, (void*)state->cr );

    if ( start_glyph_id == end_glyph_id )
    {
      cairo_scale( state->cr,
                   x_svg_to_out,
                   y_svg_to_out );
      cairo_translate( state->cr,
                       -1 * bbox_svg.x,
                       -1 *  bbox_svg.y );
      svgnative_hive_render_installed_document( hive );
    }

    cairo_surface_flush( state->surface );
    width  = cairo_image_surface_get_width( state->surface );
    height = cairo_image_surface_get_height( state->surface );
    pitch  = cairo_image_surface_get_stride( state->surface );
    slot->bitmap_left = bbox_svg.x * x_svg_to_out;
    slot->bitmap_top  = bbox_svg.y * y_svg_to_out * -1;
    slot->bitmap.rows   = height;
    slot->bitmap.width  = width;
    slot->bitmap.pitch  = pitch;

    slot->bitmap.pixel_mode = FT_PIXEL_MODE_BGRA;
    slot->bitmap.num_grays  = 256;
    slot->internal->flags |= FT_GLYPH_OWN_BITMAP;
    cairo_destroy( state->cr );
    cairo_surface_flush( state->surface );
    cairo_surface_finish( state->surface );
    cairo_surface_destroy( state->surface );
    svgnative_hive_destroy( hive );
    return FT_Err_Ok;
  }

  FT_UInt
  svgnative_port_get_state_size()
  {
    return sizeof( Svgnative_Port_StateRec );
  }

  FT_ULong
  svgnative_port_get_buffer_size( FT_GlyphSlot  slot,
                                  FT_BBox       bbox )
  {
    FT_Size_Metrics  metrics;
    FT_UShort        units_per_EM;
    FT_SVG_Document  document = (FT_SVG_Document)slot->other;

    float  x_outline_to_out;
    float  y_outline_to_out;
    int    width_final;
    int    height_final;
    int    stride;

    metrics      = document->metrics;
    units_per_EM = document->units_per_EM;

    x_outline_to_out = (float)metrics.x_ppem/(float)units_per_EM;
    y_outline_to_out = (float)metrics.y_ppem/(float)units_per_EM;

    width_final  = ceil( ( bbox.xMax - bbox.xMin ) * x_outline_to_out );
    height_final = ceil( ( bbox.yMax - bbox.yMin ) * y_outline_to_out );

    stride = cairo_format_stride_for_width( CAIRO_FORMAT_ARGB32, width_final );

    return stride * height_final;
  }
