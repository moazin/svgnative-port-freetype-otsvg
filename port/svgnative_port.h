#include <cairo.h>
#include <ft2build.h>
#include FT_FREETYPE_H

  typedef struct Svgnative_Port_StateRec_
  {
    cairo_t            *cr;
    cairo_surface_t    *surface;
  } Svgnative_Port_StateRec;

  typedef struct Svgnative_Port_StateRec_*  Svgnative_Port_State;

  FT_Error
  svgnative_port_init( FT_Library  library );

  void
  svgnative_port_free( FT_Library  library  );

  FT_Error
  svgnative_port_render( FT_GlyphSlot  slot, FT_BBox  outline_bbox );

  FT_UInt
  svgnative_port_get_state_size( );

  FT_ULong
  svgnative_port_get_buffer_size( FT_GlyphSlot  slot,
                                  FT_BBox       bbox );
